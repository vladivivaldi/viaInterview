#!/bin/sh
export TERM=xterm
mkdir -p logs
while true
do
  NOW=$(date +"%Y-%m-%d_%H-%M-%S")
  LOGFILE="./logs/net-stat-$NOW.log"
  DEFAULT_NETWORK=$(route | grep default | awk '{ print $8}')
  CURRENT_NETWORK_CONFIG=$(ifconfig $DEFAULT_NETWORK)

  RX_PACKETS=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'RX packets' | awk '{ print $3}')
  RX_BYTES=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'RX.*bytes' | awk '{ print $5}')
  RX_ERRORS=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'RX errors' | awk '{ print $3}')
  TX_PACKETS=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'TX packets' | awk '{ print $3}')
  TX_BYTES=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'TX.*bytes' | awk '{ print $5}')
  TX_ERRORS=$(echo "$CURRENT_NETWORK_CONFIG" | grep 'TX errors' | awk '{ print $3}') 

  echo '{'> $LOGFILE
  echo "  \"DEFAULT_NETWORK\": \"$DEFAULT_NETWORK\",">> $LOGFILE
  echo "  \"RX_PACKETS\": $RX_PACKETS,">> $LOGFILE
  echo "  \"RX_BYTES\": $RX_BYTES,">> $LOGFILE
  echo "  \"RX_ERRORS\": $RX_ERRORS,">> $LOGFILE
  echo "  \"TX_PACKETS\": $TX_PACKETS,">> $LOGFILE
  echo "  \"TX_BYTES\": $TX_BYTES,">> $LOGFILE
  echo "  \"TX_ERRORS\": $TX_ERRORS">> $LOGFILE
  echo '}'>> $LOGFILE

  echo "$(cat $LOGFILE)" > index.html
  clear && cat $LOGFILE

  sleep $CHECK_INTERVAL
done
