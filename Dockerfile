FROM centos:7
LABEL maintainer=Vladev
ENV CHECK_INTERVAL=30

RUN mkdir /net-stat-logger &&\
 yum -y update &&\
  yum -y install net-tools &&\
   groupadd -r logger &&\
    useradd -r -s /bin/false -g logger logger
    
WORKDIR /net-stat-logger
COPY getInfo.sh /net-stat-logger
RUN chown -R logger:logger /net-stat-logger
USER logger

ENTRYPOINT ["/net-stat-logger/getInfo.sh"]